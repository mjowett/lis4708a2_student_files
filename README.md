Required: First, delete these notes, and include your own, before pushing
to your remote repo. This file *must* use Markdown syntax, and provide
appropriate documentation--otherwise, points *will* be deducted.
